<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MarvelCharacterPreviewController extends Controller
{
    /**
     * @Route("/marvel/character/preview", name="marvel_character_preview")
     */
    public function index()
    {
        $MarvelCharactersData = $this->CallMarvelCharactersAPI('?offset=100&ts=1&apikey=10a54b1c1181dcb872ce0e30e6d2828e&hash=ac058c66b4536c6e302ccb26fa5e0e9f');
        $fetchOnlyMarvelCharacters = $MarvelCharactersData->data->results;
        dump($fetchOnlyMarvelCharacters);
        return $this->render('marvel_character_preview/index.html.twig', [
            'MarvelCharactersData' => $fetchOnlyMarvelCharacters,
        ]);
    }

    public function CallMarvelCharactersAPI($param)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://gateway.marvel.com/v1/public/characters'.$param);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);

        $data = json_decode($response);

        return $data;
    }

    /**
     * @Route("/marvel/character/preview/{characterId}", name="marvel_character_preview_id", options={"expose": true})
     */
    public function CallMarvelCharacterByIdAPI($characterId)
    {
        $MarvelCharacterData = $this->CallMarvelCharactersAPI('/'.$characterId.'?ts=1&apikey=10a54b1c1181dcb872ce0e30e6d2828e&hash=ac058c66b4536c6e302ccb26fa5e0e9f');
        dump($MarvelCharacterData);

        return new JsonResponse($MarvelCharacterData);
    }

}
