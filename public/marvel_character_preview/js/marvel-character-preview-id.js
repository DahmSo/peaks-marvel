function getThisMarvelCharacterData(characterid){
    $.ajax({
        url: Routing.generate('marvel_character_preview_id', {characterId: characterid})
    })
        .done(function (data) {
           console.log(data);
           var characterModalInfo = document.querySelector('#centralModalCharacterInfo');
           $(characterModalInfo).show();
        });

}